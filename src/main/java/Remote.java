public interface Remote {
    void power();

    void timeUp();

    void timeDown();
}
