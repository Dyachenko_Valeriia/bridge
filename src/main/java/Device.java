public interface Device {
    boolean isEnabled(); // проверить включен или нет

    void enable(); //включить

    void disable(); //выключить

    int getTime(); //посмотреть через сколько минут сработае таймер

    void setTime(int time);

    boolean checkTimer(); //проверить, стоит ли таймер
}
