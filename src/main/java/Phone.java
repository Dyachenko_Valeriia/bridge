public class Phone implements Remote {
    private Device device;

    public Phone(Device device) {
        this.device = device;
    }

    @Override
    public void power() {
        if (device.isEnabled()) {
            device.disable();
        } else {
            device.enable();
        }
    }

    @Override
    public void timeUp() {
        device.setTime(device.getTime() + 10);
    }

    @Override
    public void timeDown() {
        device.setTime(device.getTime() - 10);
    }
}
