import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestDevice {
    @Test
    public void testPhone(){
        Device device = new Lighting();
        Phone phone = new Phone(device);
        phone.power();
        assertTrue(device.isEnabled());
    }

    @Test
    public void testComputer(){
        Device device = new Kettle();
        Phone phone = new Phone(device);
        device.setTime(100);
        phone.timeUp();
        assertEquals(110, device.getTime());
    }

}
